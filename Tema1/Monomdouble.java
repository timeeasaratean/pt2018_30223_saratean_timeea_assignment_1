package temaPolinoame;


public class Monomdouble {
	double grad;
	double coeficient; 
	
	public Monomdouble(double coef,double gr) {
		this.coeficient=coef;
		this.grad=gr;
	
}
	public double getGrad() {
		return this.grad;
	}
	public void setGrad(double grad) {
		this.grad=grad;
	}
	public double getCoef() {
		return this.coeficient;
	}
	public void setCoef(double coeficient) {
		this.coeficient=coeficient; 
	}
	public Monomdouble imparte(Monomdouble m) {
			Monomdouble mm=new Monomdouble(0,0);
			if (m.getCoef()!=0) 
        	mm.coeficient=this.coeficient/m.coeficient;
        	mm.grad=this.grad-m.grad;
        	return mm;
        	
    }
	public Monomdouble inmultire(Monomdouble m) {
		Monomdouble mm=new Monomdouble(0,0);
    	mm.coeficient=this.coeficient*m.coeficient;
    	mm.grad=this.grad+m.grad;
    	return mm;
    	
}
	

}
