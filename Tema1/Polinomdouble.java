package temaPolinoame;



import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class Polinomdouble{
	 public ArrayList<Monomdouble> list1=new ArrayList<Monomdouble>();
	 
	 
	 public Polinomdouble(String s) {
		 this.list1=new ArrayList<Monomdouble>();
	     list1 = parsePolinom(s);
	}
	 
	 public  Polinomdouble(){
		 
	        list1=new ArrayList<Monomdouble>();
	    }
	    void adaugaMonom2(double coeficient, double grad) {
	    	Monomdouble m=new Monomdouble(0,0);
	    	m.setCoef(coeficient);
	    	m.setGrad(grad);
	    	list1.add(m);
	    	
	    }
	   public  Monomdouble getMonomMax() {
	    	double g=0,c=0;
	    	Monomdouble m=new Monomdouble(0,0);
	    	for(Monomdouble i:list1)
	    	{
	    		if (i.getGrad()>g)
	    		    {
	    			g=i.getGrad();
	    			c=i.getCoef();
	    		    }
	    	}
	    	m.setGrad(g);
	    	m.setCoef(c);
	    	
	    	return m;
	    }
	  
	    public Polinomdouble multiply(Monomdouble m) {
	    	Polinomdouble p=new Polinomdouble();
	    	for (Monomdouble i:this.list1) {	
	    	    p.list1.add(i.inmultire(m));
	    	}
	    	return p;
	    }
	    int gradPolinom(int i){
		      return this.list1.size()-1-i;
		    }
	    public  Monomdouble parseMonom(String s){
			Monomdouble m = new Monomdouble(1,0);
		    double coef = 0;
		    double gradd = 0;
		    if (s.startsWith("x")){
		    	coef = 1;
		    	gradd = 1;
		    }
		    else {
		    	if (s.indexOf('x') == -1){
		    		coef = Integer.parseInt(s);
		    		gradd = 0;
		    	}
		    	else {
		    		gradd = 1;
		    		coef = Integer.parseInt(s.substring(0, s.indexOf('x')));
		    	}
		    }
		    if (s.indexOf('^') != -1){
		    	gradd = Integer.parseInt(s.substring(s.indexOf('^') + 1));
		    }
		    
		    m.setCoef(coef);
		    m.setGrad(gradd);
			return m;
		}
		public ArrayList<Monomdouble> parsePolinom(String s){
			ArrayList<Monomdouble>  p = new ArrayList<Monomdouble> ();
			Monomdouble m = new Monomdouble(0,0);
			Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
			Matcher matcher = pattern.matcher(s);
			while (matcher.find()) {
					m = parseMonom(matcher.group(1));
					p.add(m);
			}
			return p;
		}
		 public String afisarePolinom(){
	    	 String p=new String();
	    	 int j=0;
	    	 if (list1.isEmpty()) {
	    		 return null;
	    	 }
	    	for(Monomdouble i:list1) {
	    	 if(i.getCoef()!=0) {
	    		 if(j!=0) {
	    		 if ((i.getCoef())>0) {
	    		 p=p+"+"+i.getCoef()+"x^"+i.getGrad();
	    		 }
	    	     else {
	    	    	p=p+i.getCoef()+"x^"+i.getGrad(); 
	    	     }
	    		 }
	    		 else {
	    			 p=i.getCoef()+"x^"+i.getGrad();
	    		 }
	    		 }
	    	 j++;
	    		
	    	}
	    	return p;
	    	}	    
	    public Polinomdouble scade(Polinomdouble p) {
			Polinomdouble result = new Polinomdouble();
			Polinomdouble aux=new Polinomdouble();
			Polinomdouble aux2=new Polinomdouble();
			aux.list1.addAll(list1);
			aux2.list1.addAll(p.list1);
			for (Monomdouble i:list1) {
				for (Monomdouble j:p.list1) {
					if (i.getGrad()==j.getGrad()) {
						Monomdouble m=new Monomdouble(0,0);
						m.setCoef(i.getCoef()-j.getCoef());
						m.setGrad(i.getGrad());
						result.list1.add(m);
						aux.list1.remove(i);
						aux2.list1.remove(j);
					}
				}
			}
			
		for(Monomdouble m1:aux.list1) {
			result.list1.add(m1);
			}
		 		
		 for (int i=0;i<aux2.list1.size();i++) {
			 Monomdouble m=new Monomdouble(-aux2.list1.get(i).getCoef(),aux2.list1.get(i).getGrad());
			  result.list1.add(m);
		  }
			return result;
		}

	   
	   
	    	
	    
}
