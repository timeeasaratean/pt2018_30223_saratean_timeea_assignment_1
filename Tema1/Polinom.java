package temaPolinoame;



import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class Polinom {
	 public ArrayList<Monom> list=new ArrayList<Monom>();
	 
	 
	 public Polinom(String s) {
		 this.list=new ArrayList<Monom>();
	     list = parsePolinom(s);
	}
	 
	 public  Polinom(){
		 
	        list=new ArrayList<Monom>();
	    }
	    void adaugaMonom(int coeficient, int grad) {
	    	Monom m=new Monom(0,0);
	    	m.setCoef(coeficient);
	    	m.setGrad(grad);
	    	 list.add(m);
	    }

	    public  Monom getMonomMax() {
	    	int grad=0,coef=0;
	    	//Monom m=new Monom(0,0);
	    	for(Monom i:list)
	    	{
	    		if (i.getGrad()>grad)
	    		    {
	    			grad=i.getGrad();
	    			coef=i.getCoef();
	    		    }
	    	}
	    	Monom m=new Monom(coef, grad);
	    	
	    	return m;
	    }
	    int gradPolinom(){
		      return list.size()-1;
		    }
	   
	  
	    public  Monom parseMonom(String s){
			Monom m = new Monom(1,0);
		    int coef = 0;
		    int gradd = 0;
		    if (s.startsWith("x")){
		    	coef = 1;
		    	gradd = 1;
		    }
		    else {
		    	if (s.indexOf('x') == -1){
		    		coef = Integer.parseInt(s);
		    		gradd = 0;
		    	}
		    	else {
		    		gradd = 1;
		    		coef = Integer.parseInt(s.substring(0, s.indexOf('x')));
		    	}
		    }
		    if (s.indexOf('^') != -1){
		    	gradd = Integer.parseInt(s.substring(s.indexOf('^') + 1));
		    }
		    
		    m.setCoef(coef);
		    m.setGrad(gradd);
			return m;
		}
		
		public ArrayList<Monom> parsePolinom(String s){
			ArrayList<Monom>  p = new ArrayList<Monom> ();
			Monom m = new Monom(0,0);
			Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
			Matcher matcher = pattern.matcher(s);
			while (matcher.find()) {
					m = parseMonom(matcher.group(1));
					p.add(m);
			}
			
			return p;
		}
	   public String afisarePolinom(){
	    	 String p=new String();
	    	 int j=0;
	    	 if (list.isEmpty()) {
	    		 return null;
	    	 }
	    	for(Monom i:list) {
	    	 if(i.getCoef()!=0) {
	    		 if(j!=0) {
	    		 if ((i.getCoef())>0) {
	    		 p=p+"+"+i.getCoef()+"x^"+i.getGrad();
	    		 }
	    	     else {
	    	    	p=p+i.getCoef()+"x^"+i.getGrad(); 
	    	     }
	    		 }
	    		 else {
	    			 p=i.getCoef()+"x^"+i.getGrad();
	    		 }
	    		 }
	    	 j++;
	    		
	    	}
	    	return p;
	    	}	    
}
