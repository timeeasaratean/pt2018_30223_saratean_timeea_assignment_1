package temaPolinoame;



public class Monom {
	int grad;
	int coeficient; 
	
	public Monom(int coef,int gr) {
		this.coeficient=coef;
		this.grad=gr;
	
}
	public int getGrad() {
		return this.grad;
	}
	public void setGrad(int grad) {
		this.grad=grad;
	}
	public int getCoef() {
		return this.coeficient;
	}
	public void setCoef(int coeficient) {
		this.coeficient=coeficient; 
	}
	
	
}
