package temaPolinoame;





public class Operatii extends Polinom{

	public Polinom aduna(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		Polinom aux=new Polinom();
		Polinom aux2=new Polinom();
		aux.list.addAll(p1.list);
		aux2.list.addAll(p2.list);
		for (Monom i:p1.list) {
			for (Monom j:p2.list) {
				if (i.getGrad()==j.getGrad()) {
					Monom m=new Monom(0,0);
					m.setCoef(i.getCoef()+j.getCoef());
					m.setGrad(i.getGrad());
					result.list.add(m);
					aux.list.remove(i);
					aux2.list.remove(j);
				}
			}
		}
		
	for(Monom m1:aux.list) {
		result.list.add(m1);
		}
	 		
	 for ( Monom m2:aux2.list) {
		result.list.add(m2);
	 }
		return result;
	}

	public Polinom scade(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		Polinom aux=new Polinom();
		Polinom aux2=new Polinom();
		aux.list.addAll(p1.list);
		aux2.list.addAll(p2.list);
		for (Monom i:p1.list) {
			for (Monom j:p2.list) {
				if (i.getGrad()==j.getGrad()) {
					Monom m=new Monom(0,0);
					m.setCoef(i.getCoef()-j.getCoef());
					m.setGrad(i.getGrad());
					result.list.add(m);
					aux.list.remove(i);
					aux2.list.remove(j);
				}
			}
		}
		
	for(Monom m1:aux.list) {
		result.list.add(m1);
		}
	 		
	 for (int i=0;i<aux2.list.size();i++) {
		 Monom m=new Monom(-aux2.list.get(i).getCoef(),aux2.list.get(i).getGrad());
		  result.list.add(m);
	  }
		return result;
	}
	public Polinom inmulteste(Polinom p1, Polinom p2) {
		Polinom mul=new Polinom();
		for(Monom i:p1.list) {
	    	for(Monom j:p2.list)
	    	{ 
		    		Monom m=new Monom(0,0);
		    		m.setGrad(i.getGrad()+j.getGrad());
		    		m.setCoef(i.getCoef()*j.getCoef());
		    	    mul.list.add(m);	
	    	}
		}
	    for(int i=0;i<mul.list.size();i++) {
	    	for(int j=i+1;j<mul.list.size();j++)
	    	{
	    		if(mul.list.get(i).getGrad()==mul.list.get(j).getGrad())
	    		{
	    			mul.list.get(i).setCoef(mul.list.get(i).getCoef()+mul.list.get(j).getCoef());
	    			mul.list.remove(mul.list.get(j));
	    			i=0;
	    		}
	    	}
	    	}
		return mul;
	}
	public Polinom derivare(Polinom p) {
		Polinom deri=new Polinom();
		int gr, coef;
		for (Monom d:p.list) {
			coef=(d.getCoef()*d.getGrad());
			gr=d.getGrad()-1;
			Monom m=new Monom(coef,gr);
			deri.list.add(m);
		}
		return deri;
	}
	public Polinomdouble integrare(Polinomdouble p) {
		Polinomdouble integr=new Polinomdouble();
		double  coef;
		double gr;
		for(int i=0; i<p.list1.size(); i++) {
			coef=p.list1.get(i).getCoef()/(p.list1.get(i).getGrad()+1);
			gr= p.list1.get(i).getGrad()+1;
			integr.adaugaMonom2(coef,gr); 
		}
		return integr;
	}
	public Polinomdouble impartire(Polinomdouble p1, Polinomdouble p2,Polinomdouble rest) {
		Polinomdouble div=new Polinomdouble();
		int i=0;
		//Polinomdouble rest=new Polinomdouble();
		while(p2.gradPolinom(0)<p1.gradPolinom(i)) {
			Monomdouble m1=new Monomdouble(0,0);
			m1.setGrad(p1.list1.get(i).getGrad());
			m1.setCoef(p1.list1.get(i).getCoef());
			Monomdouble m2=new Monomdouble(0,0);
			m2.setGrad(p2.list1.get(0).getGrad());
			m2.setCoef(p2.list1.get(0).getCoef());
			Monomdouble m3=m1.imparte(m2);   
			div.list1.add(m3);
			Polinomdouble cat=p2.multiply(m3);
			p1=p1.scade(cat);
			i++;
		}
		Monomdouble m1=new Monomdouble(0,0);
		m1.setGrad(p1.list1.get(i).getGrad());
		m1.setCoef(p1.list1.get(i).getCoef());
		Monomdouble m2=new Monomdouble(0,0);
		m2.setGrad(p2.list1.get(0).getGrad());
		m2.setCoef(p2.list1.get(0).getCoef());
		Monomdouble m3=m1.imparte(m2);   
		div.list1.add(m3);
		Polinomdouble cat=p2.multiply(m3);
		p1=p1.scade(cat);
		rest.list1.addAll(p1.list1);
		return div;
		
	}
	public String Afis(Polinom p){
		return p.afisarePolinom();
	}
	public String Afis1(Polinomdouble p){
		return p.afisarePolinom();
	}

}
