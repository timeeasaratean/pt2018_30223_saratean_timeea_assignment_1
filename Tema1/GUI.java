package temaPolinoame;
import javax.swing.*;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class GUI extends JFrame  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String s1;
	private String s2;
	JButton s=new JButton("Suma");
	 JButton d=new JButton("Diferenta");
	 JButton mul=new JButton("Inmultire");
	 JButton deri=new JButton("Derivare");
	 JButton inte=new JButton("Integrare");
	 JButton impa=new JButton("Impartire");
	 JLabel l1= new JLabel ("Introduceti primul polinom: ") ;
	 JTextField textField = new JTextField () ;
	 JLabel l2= new JLabel ("Introduceti al doilea polinom: ") ;
	 JTextField textFieldd = new JTextField () ;
	 JTextField textField1 = new JTextField () ;
	 JLabel l3=new JLabel ("Restul impartirii: ");
	 JTextField restt=new JTextField();
	
	public GUI() {
	 FlowLayout lay1 = new FlowLayout () ;
	 JPanel panel1 = new JPanel () ;
	 panel1.setLayout(lay1);
	 s.setPreferredSize(new Dimension(90,50));
	 d.setPreferredSize(new Dimension(90,50));
	 mul.setPreferredSize(new Dimension(90,50));
	 deri.setPreferredSize(new Dimension(90,50));
	 inte.setPreferredSize(new Dimension(90,50));
	 impa.setPreferredSize(new Dimension(90,50));
	 panel1 . add (s) ;
	 panel1 . add (d) ;
	 panel1 . add (mul ) ;
	 panel1. add (deri ) ;
	 panel1 . add (inte ) ;
	 panel1.add(impa);
	 JPanel panel2= new JPanel();
	 l1.setPreferredSize(new Dimension(200,50));
	  panel2. add ( l1 , BorderLayout.PAGE_START ) ;
	 textField . setPreferredSize ( new Dimension (200 , 30) ) ;
	 panel2 . add ( textField , BorderLayout . CENTER ) ;
	 JPanel panel3=new JPanel();
	 l2.setPreferredSize(new Dimension(200,50));
	 panel3. add ( l2 , BorderLayout . PAGE_START ) ;
     textFieldd . setPreferredSize ( new Dimension (200 , 30) ) ;
	 panel3 . add ( textFieldd , BorderLayout . CENTER ) ;

    JPanel P1=new JPanel();
    P1.setLayout(new BoxLayout(P1,BoxLayout.LINE_AXIS));
    P1.add(panel2);
    P1.add(panel3);
    
  
    textField1 . setPreferredSize ( new Dimension (200 , 30) ) ;
    JPanel panel4=new JPanel();
    panel4.setLayout(lay1);
    panel4.add(new JLabel("Rezultat: "));
	 panel4.add(textField1);
    
    restt.setPreferredSize(new Dimension(150,30));
    JPanel panel5=new JPanel();
    panel5.setLayout(lay1);
    panel5.add(new JLabel("Rest impartire: "));
    panel5.add(restt);


	 
	 JPanel panel = new JPanel () ;
	 panel . setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS) ) ;
	panel.add(P1);
	panel.add(panel4);
	panel.add(panel5);
	 panel . add ( panel1 ) ;
	 
	 add(panel);
	 
	 s.addActionListener(new Suma());
	 d.addActionListener(new Diferenta());
	 deri.addActionListener(new Derivare());
	 inte.addActionListener(new Integrare());
	 mul.addActionListener(new Inmultire());
	 impa.addActionListener(new Impartire());
	 
	 
	 
	
}
	
	 
	class Suma implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
	    	s2=textFieldd.getText();
	    	String a=new String();
	    	Polinom p1=new Polinom(s1);	
	        Polinom p2=new Polinom(s2); 
	        Operatii op=new Operatii();
	        a=op.Afis(op.aduna(p1, p2));
	        textField1.setText(a);
        }
    }
	class Diferenta implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
	    	s2=textFieldd.getText();
	    	String a=new String();
	    	Polinom p11=new Polinom(s1);	
	        Polinom p21=new Polinom(s2);   
	        Operatii p31=new Operatii();
	        a=p31.Afis(p31.scade(p11, p21));
	         textField1.setText(a);
        }
    }
	class Derivare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
        	 String d=new String();
 	    	Polinom p14=new Polinom(s1);	 
 	        Operatii p34=new Operatii();
 	        d=p34.Afis(p34.derivare(p14));
 	        textField1.setText(d);
        }
    }
	
	class Integrare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
        	 String i=new String();
 	    	Polinomdouble p15=new Polinomdouble(s1);	 
 	        Operatii p35=new Operatii();
 	        i=p35.Afis1(p35.integrare(p15));
 	        textField1.setText(i);
        }
    }
	class Inmultire implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
	    	s2=textFieldd.getText();
	    	String in=new String();
	    	Polinom p12=new Polinom(s1);	
	        Polinom p23=new Polinom(s2);   
	        Operatii p33=new Operatii();
	        in=p33.Afis(p33.inmulteste(p12, p23));
	        
	    	
	        textField1.setText(in);
        }
    }
	class Impartire implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	s1=textField.getText();
	    	s2=textFieldd.getText();
	    	String im=new String();
	    	String re=new String();
	    	Polinomdouble p16=new Polinomdouble(s1);	
	        Polinomdouble p26=new Polinomdouble(s2); 
	        Polinomdouble rest=new Polinomdouble();
	        Operatii p36=new Operatii();
	        im=p36.Afis1(p36.impartire(p16, p26,rest));	    
	        re=rest.afisarePolinom();
	    	
	        textField1.setText(im);
	        restt.setText(re);
        }
    }

	 public static void main(String[] args) {
	        // TODO Auto-generated method stub
	       GUI poly = new GUI(); 
	       poly.pack();
	       poly.setLocationRelativeTo(null);
	       poly.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	       poly.setTitle("Calculator de polinoame");
	       poly.setSize(600, 500);
	       poly.setVisible(true);
	    }
	 	
}