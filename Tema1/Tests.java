package temaPolinoame;

import org.junit.Assert;
import org.junit.Test;



public class Tests {

	

		@Test
			public void testCitire() {
				String expectedString=new String();
				expectedString="9x^4-3x^3+5x^2-1x^0";
				Polinom p= new Polinom(expectedString);
				String actualString= new String();
				actualString=p.afisarePolinom();
				Assert.assertEquals(expectedString, actualString); 
			}
		
		@Test
		public void testGasireaMonomMaximDinPolinom(){
			String s=new String();
			s="7x^4-9x^2+10x^1-1x^0";
			Polinom p= new Polinom(s);
			Monom expectedMonom=new Monom(7,4);
			Monom actualMonom=p.getMonomMax();
			Assert.assertEquals(expectedMonom.coeficient, actualMonom.coeficient);
			Assert.assertEquals(expectedMonom.grad,actualMonom.grad);
			
		}
		@Test
		public void testAdunare() {
			String s1=new String();
			String s2=new String();
			s1="3x^2-1x^0";
			s2="1x^2-1x^1";
			String actualString=new String();
			String expectedString=new String();
			expectedString="4x^2-1x^0-1x^1";
		    Polinom p1=new Polinom(s1);	
		    Polinom p2=new Polinom(s2);
		    Operatii p3=new Operatii();	   
		    actualString=p3.Afis((p3.aduna(p1,p2)));
		    Assert.assertEquals(expectedString, actualString);
		}
		@Test 
		public void testScadere() {
			String s1=new String();
			String s2=new String();
			s1="3x^3+2x^2+2x^0";
			s2="1x^2+4x^0";
			String actualString=new String();
			String expectedString=new String();
			expectedString="1x^2-2x^0+3x^3";
			Polinom p1=new Polinom(s1);	
		    Polinom p2=new Polinom(s2);
		    Operatii p3=new Operatii();
		    actualString=p3.Afis(p3.scade(p1, p2));
		    Assert.assertEquals(expectedString, actualString);
		}
		
		@Test
		public void testInmultire() {
			String s1=new String();
			String s2=new String();
			s1="2x^2+2x^0";
			s2="1x^2+4x^0";
			String actualString=new String();
			String expectedString=new String();
			expectedString="2x^4+10x^2+8x^0";
			Polinom p1=new Polinom(s1);	
		    Polinom p2=new Polinom(s2);
		    Operatii p3=new Operatii();
		    actualString=p3.Afis(p3.inmulteste(p1, p2));
		    Assert.assertEquals(expectedString, actualString);
		}
		
		@Test
		public void testDerivare() {
			String s1=new String();
			s1="2x^3+2x^1";
			String actualString=new String();
			String expectedString=new String();
			expectedString="6x^2+2x^0";
			Polinom p1=new Polinom(s1);	
		    Operatii p3=new Operatii();
		    actualString=p3.Afis(p3.derivare(p1));
		    Assert.assertEquals(expectedString, actualString);
		}
		
		@Test
		public void testIntegrare() {
			String s1=new String();
			s1="2x^3+2x^1";
			String actualString=new String();
			String expectedString=new String();
			expectedString="0.5x^4.0+1.0x^2.0";
			Polinomdouble p1=new Polinomdouble(s1);	
		    Operatii p3=new Operatii();
		    actualString=p3.Afis1(p3.integrare(p1));
		    Assert.assertEquals(expectedString, actualString);
		}
		

	}


